package com.mcfht.rfo;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mcfht.rfo.scheduler.FluidUpdater;
import com.mcfht.rfo.utils.Util;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;

public class RFOCommands extends CommandBase 
{
	
	//List<String> aliases = Util.makeList("fluid", "fluids", "ff", "finitefluids");
	

	public RFOCommands() {	
		
		//simplest settings
		//new Setting("FLOW_ENABLED", "flow", "Fluid processing", "true/false"); 
		
		
		new Setting("PRESSURE_ENABLED", "pressure", "Pressure processing", "true/false"); 
		
		new Setting("UPDATE_REDUCTION_FALLOFF", "sendFactor", "Sending Factor", "decimal");
		
		new Setting("TICK_CURRENT", "rate", "Update rate", "integer. Higher = slower");
		
		new Setting("LOAD_FACTOR", "load", "Load allowance", "decimal") {
			@Override public void process() { Settings.LOAD_MULT = (int) (50.F *Settings.LOAD_FACTOR); } };
			
		new Setting("RANGE_FAR_ABS", "far", "Far update range", "integer") {
			@Override public void process(){ Settings.RANGE_NEAR = Settings.RANGE_FAR_ABS * Settings.RANGE_FAR_ABS; } };
		
		new Setting("RANGE_NEAR_ABS", "near", "Near update range", "integer") {
			@Override public void process() { Settings.RANGE_NEAR = Settings.RANGE_NEAR_ABS * Settings.RANGE_NEAR_ABS; } };
		
		new Setting("UPDATER_COUNT", "threads", "Number of threads", "integer") {
			@Override public void process() { Settings.UPDATER_COUNT = Math.max(1, Settings.UPDATER_COUNT);} };
			
		new Setting("EQUALIZE_NATURAL", "eq", "Natural equalization", "true/false");
		
		new Setting("EQUALIZE_NATURAL_TENDENCY", "eqrate", "Natural equalization Tendency", "decimal");

	}
	
	@Override
	public String getCommandName() {
		return "fluids";
	}
	
	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		return  "Controls fluid settings. Usage: /fluid [setting] [val] => sets new value for this setting."
				+ " (omit [val] to get current value). Use /fluid help for list of settings.";
	}

	/** For convenience, sends a string to ALL players via chat*/
	public static void showAll(String text)
	{
		MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new ChatComponentText(text));
	}
	
	public static class Setting
	{
		
		static Map<String, Setting> settings = new HashMap<String, Setting>();
		
		/** The class containing the value to be updated */
		Class c;
		/** The field in {@linkplain #c} corresponding to this entry */
		Field F;
		/** The name of this setting that the user has to type in */
		String keyName;
		/** More verbose textual description of this setting */
		String displayName;
		/** A brief description of this setting */
		String desc = "";


		public Setting(Class C, String fieldname, String inputName, String DisplayName, String comment)
		{
			c = C; 
			
			if (comment != null && comment.length() > 0) this.desc = " (" + comment + ")";
			this.keyName = inputName;
			displayName = DisplayName;
			try {
				F = c.getDeclaredField(fieldname);
				settings.put(keyName.toLowerCase(), this);
			} catch (Exception e) {Util.error(" -> Command creation failed: " + inputName + "(" + C.getName() + ", " +  fieldname + ")"); }
			
		}
		
		public Setting(String fieldname, String inputName, String displayName, String comment)
		{
			this(Settings.class, fieldname, inputName, displayName, comment);
		}
		
		public Setting(String fieldname, String inputName, String DisplayName)
		{
			this(fieldname, inputName, DisplayName, "");
		}
		
		/** Called after setting is changed. Gives a chance to do any additional processing. */
		public void process() { }
		
	}
	
	//Need to have moderator powerz
	@Override
    public int getRequiredPermissionLevel()
    {
        return 2;
    }
	
	public void set(ICommandSender sender, String name, String[] params)
	{
		
		//handle some basic commands
		//pause and resume
		if (name.equals("start"))
		{
			if (Settings.FLOW_ENABLED == false) showAll(" Fluid processing has been enabled!");
			Settings.FLOW_ENABLED = true;
			return;
		}
		else if (name.equals("stop"))
		{
			if (Settings.FLOW_ENABLED) showAll(" Fluid processing has been disabled!");
			Settings.FLOW_ENABLED = false;
			return;
		}
		//help
		else if (name.equals("help"))
		{
			sender.addChatMessage(new ChatComponentText("Commands:"));
			sender.addChatMessage(new ChatComponentText("Start/Stop : pauses/resumes the entire mod"));
			sender.addChatMessage(new ChatComponentText(" "));
			sender.addChatMessage(new ChatComponentText("Settings:"));
			
			String s = "";
			
			for (Setting setting : Setting.settings.values())
			{
				try {s += setting.keyName + setting.desc + ", ";}
				catch (Exception e) { }
			}
			
			sender.addChatMessage(new ChatComponentText(s.substring(0, s.length() - 2)));
			return;
		}
		//basic stats
		else if (name.equals("stats"))
		{
			if (params.length >= 2)
			{
				
				if (params.length > 2 || !params[1].equals("reset"))
				{
					sender.addChatMessage(new ChatComponentText("Invalid parameters!"));
				}
				else
				{
					FluidUpdater.chunksDone.set(0);
					RFO.sweepCounter = 0;
					sender.addChatMessage(new ChatComponentText("Stats reset"));
				}
				return;
			}
			
			
			sender.addChatMessage(new ChatComponentText(" Stats;"));
			sender.addChatMessage(new ChatComponentText(" Chunk Updates: " + FluidUpdater.chunksDone.get() + ", Updates sweeps: " + RFO.sweepCounter));
			sender.addChatMessage(new ChatComponentText(" Average: " + (FluidUpdater.chunksDone.get()/Math.max(1, RFO.sweepCounter))));
			return;
		}
		

		Setting S = Setting.settings.get(name.toLowerCase());
		if (S == null) 
		{
			sender.addChatMessage(new ChatComponentText("No such setting found."));
			return;
		}
		
		//the user only typed a setting name, spit out the value
		if (params.length <= 1)
		{
			try {
				//
				Object val = S.F.get(S.c);
				
				//because we can
				if (val.toString().equals("true")) val = "enabled";
				if (val.toString().equals("false")) val = "disabled";
				
				sender.addChatMessage(new ChatComponentText(S.displayName + " is " + val + S.desc));
			} catch (Exception e) { sender.addChatMessage(new ChatComponentText("Invalid arguments!")); } 
			return;
		}
		
		//there are more arguments than this command accepts
		if (params.length > 2)
		{
			sender.addChatMessage(new ChatComponentText("Too many arguments!"));
			return;
		}
		
		//We have a second parameter
		String s = params[1];
		Object value = s; //the value we will read from s
		Object _value = null; //the existing field value
		
		try { _value = S.F.get(null);
		} catch (Exception e) { }
		
		//Assumed: Only going to be updating float, int, or boolean settings
		try { 
				 if (S.F.getType().equals(boolean.class)) value = parseBoolean(sender, s);
			else if (S.F.getType().equals(int.class)) value = parseInt(sender, s);
			else if (S.F.getType().equals(float.class)) value = (float)parseDouble(sender, s);
				 
			//try and apply the changes
			S.F.set(null, value); 
			S.process(); //any post processing
		} 
		//we failed, just tell the user a generic error message
		catch (Exception e) { sender.addChatMessage(new ChatComponentText("Invalid arguments!")); return; }
		
		//if the original value was not null (aka we did not fail)
		if (_value != null)
		{
			//if the value changed, tell everybody
			if (!_value.equals(value)) showAll(S.displayName + " was set to " + value + S.desc);
			//otherwise, just inform the user who changed it
			else sender.addChatMessage(new ChatComponentText(S.displayName + " was set to " + value + S.desc));
			
			Settings.resetConfig();
		}
		else
		{
			//or tell the user they failed
			sender.addChatMessage(new ChatComponentText("Invalid arguments!"));
		}
	}
	
	@Override
	public void processCommand(ICommandSender sender, String[] comStr) 
	{
		if (comStr.length > 0)
		{
			//idk why I did it like this. Whatever.
			set(sender, comStr[0].toLowerCase(), comStr);
		}
		else
		{
			sender.addChatMessage(new ChatComponentText("Finite Fluids is " + (Settings.FLOW_ENABLED ? "running." : "not running.") + " Type '/fluids help' for help with this command."));
		}
	}

}
