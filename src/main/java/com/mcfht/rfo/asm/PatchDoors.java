package com.mcfht.rfo.asm;

import java.util.Iterator;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodNode;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.asm.ASMTransformer.PatchTask;

/**
 * This makes doors throw block updates when opened and closed.
 * @author FHT
 */
public class PatchDoors extends PatchTask 
{

	protected PatchDoors(int type) {
		super(-1, NAMES[type], Settings.PATCH_DOORS);
	}

	public static final String[][] NAMES = {
			{
				"net.minecraft.block.BlockDoor",
				"akn"
			},
			{
				"net.minecraft.block.BlockTrapDoor",
				"aoe"
			}
	};
	

	@Override
	public void doPatch(String name, ClassNode c, boolean obf)
	{
		for (MethodNode m : c.methods)
		{
			if (!m.desc.contains("III")) continue;
			AbstractInsnNode node0 = null;
			@SuppressWarnings("unchecked")
			Iterator<AbstractInsnNode> iter = m.instructions.iterator();
			int index = -1;
			while (iter.hasNext())
			{
				index++;
				node0 = iter.next();
				
				if (node0.getOpcode() == Opcodes.ICONST_2 && iter.hasNext())
				{
					index++;
					if (iter.next().getOpcode() == Opcodes.INVOKEVIRTUAL)
					{
						m.instructions.set(node0, new InsnNode(Opcodes.ICONST_3));
						continue;
					}
				}
			}
		}
	}
	
	
	
	
	
}
