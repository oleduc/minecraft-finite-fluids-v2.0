package com.mcfht.rfo.datastructure;

import java.lang.annotation.Inherited;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Observer;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.WorldEvent;

public class DataInterfaces 
{


	/**
	 * Manager instance exists for the entire MC instance. Is required to exist on server. Contains
	 * all of the {@linkplain IWorld} instances registered to each Minecraft world. These instances should also
	 * be responsible for registering those worlds.
	 * 
	 * @author FHT
	 */
	public static interface IManager
	{
		
		/** Gets the {@linkplain IWorld} for the given MC World, creates a new one if it is not registered. */
		public IWorld getOrRegisterWorld(World w);
		/** Gets the {@linkplain IWorld} for the given MC World, returns null if it is not registered. */
		public IWorld getWorld(World w);
		/** Deregisters the {@linkplain IWorld} for the given MC World. */
		public IWorld deregisterWorld(World w);
		
		/** A collection containing all IWorlds linked to this instance */
		public Collection<IWorld> getWorlds();
	}
	
	public static interface IWorld
	{
		/** Gets the world associated with this IWorld Instance. */
		public World getWorld();
		
		/** Gets a hashmap of domains associated with this IWorld */
		public Collection<IChunk> getChunks();
		
		/** Gets or registers a chunk with this world object */
		public IChunk getOrRegisterChunk(Chunk c);	
		
		/** Tries to deregister chunk with world object */
		public IChunk deregisterChunk(Chunk c);	
		
		/** Gets the chunk at the given chunk position, registering it if it does not exist. */
		public IChunk getChunk(Chunk c);
		
		/** Gets the chunk at the given chunk position, registering it if it does not exist. */
		public IChunk getChunk(int xPos, int zPos);
		
		/** Gets the chunk data for this <b>WORLD</b> x/z, checking against d0. Accepts d0 == null. <b><p>FORCE IS DANGEROUS. USE WITH CARE.</b>*/
		public IChunk validateChunkData(IChunk d0, int x, int z, boolean force);
		
		/** Gets all specially registered observers for this chunk */
		public Collection<Observer> getObservers();
		
		public void registerObserver(Observer o);
		
		public void deregisterObserver(Observer o);
		
		public boolean validateObserver(Observer o);
		
		/** Sets a block. This method is not inherently threadsafe, and should inherit thread safety from the call stack.
		 * 
		 * <p><b>THIS METHOD IS VERY FAST, BUT SKIPS A LOT OF STEPS. UNLESS YOU KNOW WHAT YOU ARE DOING, DO
		 * NOT USE THIS TO SET FLUID LEVELS.</b>
		 * 
		 * <p><b>FLAGS</b>
		 * <li>0x1 => Cause fluid updates
		 * <li>0x2 => Cause lighting updates
		 * <li>0x4 => Send block changes (always uses {@linkplain WorldData#updateBlock(World, int, int, int, Block, boolean)})
		 * <li>0x8 => Update neighboring blocks ({@linkplain Block#onNeighborBlockChange(World, int, int, int, Block)})
		 */
		public void setBlock(IChunk d0, int x, int y, int z, Block b, int m, int flags);
		
		/** Drops this block as an item (deferred), then sets this block to air (update is marked immediately) */
		public void dropBlock(IChunk d0, Block b0, int x, int y, int z, int m);
		
		/** Sets a block to fluid - updates the level, the metadata, etc. b0 and m0 indicate the block that *was* here,
		 * to measure block updates, and are not necessarily essential - it is assumed that negative/null values will be retrieved.
		 * 
		 * <p>This method is not inherently threadsafe, and should inherit thread safety from the call stack.
		 * 
		 * <p><b>FLAGS</b>
		 * <li>0x1 => Cause fluid updates
		 * <li>0x2 => Cause lighting updates
		 * <li>0x4 => Send block changes
		 * <li>0x8 => Immediately send changes
		 * OR, flagging both 0x4 and 0x8 = immediate with notify neighboring blocks.
		 */
		public void setFluid(IChunk d0, int x, int y, int z, Block b0, int m0, BlockFiniteFluid f1, int l1, int flags);
		
		/** Marks neighboring blocks for a potential fluid update.
		 * 
		 * <p>It is less efficient to check the blocks as fluids now. Using this method to
		 * check blocks, means we may check most blocks several times, while checking within
		 * the updater before updating blocks, means we will only check every block once. 
		 */
		public void markNeighborFluidUpdates(final IChunk d0, final int x, final int y, final int z);
		
		/** Used to mark blocks for direct update on server.
		 * 
		 * <p>Should defer to {@linkplain ClientServerManager#flagBlockUpdate(World, int, int, int, Block, boolean)}
		 */
		public void updateBlock(World w, int x, int y, int z, Block b, boolean neighbors);
		
		/** Whether the void can drain fluids in this world/dimension */
		public boolean canVoidDrainFluids();
	}
	
	public static interface IChunk
	{
		
		/** Gets the {@linkplain IWorld} for this object */ public IWorld getIWorld();
		/** Gets the Minecraft Chunk for this data object */ public Chunk getChunk();

		/** Gets the xPos of this chunk. */ public int xPos();
		/** Gets the zPos of this chunk. */ public int zPos();
		/** Distance to closest players. */ public int getDist();
		/** Update distance from player. */ public void updateDist(int dist);

		
		/** Acquire mutex if threadsafe. */ public void acquire();
		/** Release mutex if threadsafe. */ public void release();
		/** Try get mutex if threadsafe. */ public boolean canAcquire(long timeout);
		
		/* Update stuffs */
		
		/** Whether blocks need to be sent to client. */ public boolean hasClientUpdates();
		/** Whether this chunk has any fluid updates. */ public boolean hasFluidUpdates();
		/** Ticks passed since send. Negative denies. */ public int ticksPassed();
		/** Increment a tickCounter in this chunk object. */ public void elapseTick();
		/** Prepare. Return false to prevent sending. */ public boolean prepareForSending();
		/** Chunk can send. Reset some variables etc. */ public void chunkSending();
		
		/** Updates all fluids within this IChunk. */ 
		public int performUpdates();

		/** Flags this coord for a block update to clients */
		public void flagBlockUpdate(int cx, int y, int cz);
		
		/** Gets an array flagging every block that has changed in this chunk. Encoding must follow
		 * {@link Util#getIndex12(int, int, int)}. */
		public boolean[][] getBlockFlags();
		
		/* PRESSURE STUFFS */
		
		/** Whether pressure is allowed here */ public boolean isPressureValid();
		/** Invalidates pressure arrays */ 
		public void invalidatePressure();
		
		
		/* Block getting and setting */
		
		/** Gets the ebs. If safe, then should be safe (aka, will be created thread-safely, synchronize on ebs if necessry etc) */
		public ExtendedBlockStorage getEbs(int ey, boolean safe);

		/** Get the fluid level at the coordinates. Should use this when looking for fluids. */
		public int getFluid(Block b, int m, int cx, int y, int cz);
		
		/** Lowest level metadata getter. Doesn't need to be threadsafe. */
		public int getMetadata(int cx, int y, int cz);
		
		/** Lowest level block getter. Doesn't need to be threadsafe. */
		public Block getBlock(int cx, int y, int cz);
		
		/** Lowest level metadata setter. Shouldn't cause any block updates of any kind. Doesn't need to be threadsafe. */
		public void setMetadata(int cx, int y, int cz, int m);
		
		/** Lowest level block setter. Shouldn't cause any block updates of any kind. Doesn't need to be threadsafe. */
		public void setBlock(int cx, int y, int cz, Block b, int m);
		
		/* ARRAY ACCESSORS */
		
		/** Lowest level array getter for fluid data arrays.*/
		public int getFluidData(int cx, int y, int cz);
		
		/** Lowest level array setter for fluid data arrays. */
		public void setFluidData(int cx, int y, int cz, int l);
		
		/** Lowest level array getter for fluid pressure arrays. */
		public int getPressure(int cx, int y, int cz);
		
		/** Lowest level array setter for fluid pressure arrays. */
		public void setPressure(int cx, int y, int cz, int p);
		
		/** Lowest level array getter for fluid update arrays. Gets updates for current update sweep. */
		public boolean getUpdate(int cx, int y, int cz);
		
		/** Lowest level array accessor for fluid update arrays. */
		public void setUpdate(int cx, int y, int cz, boolean update);
		
		/** Lowest level array setter for fluid update arrays. Generally only works for updates above the currently updating block. */
		public void setUpdateImmediate(int cx, int y, int cz, boolean update);

		/** Converts this chunk data into an NBT Tag compound. */
		public NBTTagCompound toNBT();
		
		/** Rebuilds this chunk data from an NBT tag compound.*/
		public void fromNBT(NBTTagCompound nbt);
		
		
	}
	
	
}
