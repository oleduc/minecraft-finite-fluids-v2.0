package com.mcfht.rfo.datastructure;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.scheduler.deferred.BlockUpdate;
import com.mcfht.rfo.scheduler.deferred.DropBlock;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Observer;
import com.mcfht.rfo.utils.Util;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class WorldData implements IWorld {

	
	
	public World w;
	public ConcurrentHashMap<Coord, IChunk> chunkCache = new ConcurrentHashMap<Coord, IChunk>(1024, 0.66F);
	public Set<Observer> observers = new HashSet<Observer>(32);

	public final boolean voidDrain;
	
	public WorldData(World w)
	{
		this.w = w;
		voidDrain = w.provider.dimensionId >= -1 && w.provider.dimensionId <= 1;
	}
	
	@Override
	public World getWorld() 
	{
		return w;
	}

	@Override
	public boolean canVoidDrainFluids()
	{
		return voidDrain;
	}
	
	@Override
	public Collection<IChunk> getChunks() 
	{
		return chunkCache.values();
	}
	
	public void registerObserver(Observer o) 
	{
		observers.remove(o);
		observers.add(o);
	}
	
	public void deregisterObserver(Observer o) 
	{
		observers.remove(o); 
	}
	
	public boolean validateObserver(Observer o) 
	{ 
		return observers.contains(o); 
	}
	
	@Override
	public Collection<Observer> getObservers() 
	{
		return observers;
	}

	

	@Override
	public IChunk getOrRegisterChunk(Chunk _c) 
	{
		IChunk C; Coord c = new Coord(_c); 
		if ((C = chunkCache.get(c)) != null) return C;
		chunkCache.put(c, C = new ChunkData(this, _c)); //change this to replace entire DS.
		return C;		
	}

	@Override
	public IChunk deregisterChunk(Chunk c) {
		if (c == null || c.isChunkLoaded) return null;
		return chunkCache.remove(new Coord(c));
	}

	@Override
	public IChunk getChunk(Chunk c) {
		return chunkCache.get(new Coord(c));	}

	@Override
	public IChunk getChunk(int xPos, int zPos) {
		return chunkCache.get(new Coord(xPos, w.provider.dimensionId, zPos));
	}

	@Override
	public IChunk validateChunkData(IChunk d0, int x, int z, boolean force) {
		return force ? forceChunkData(d0, x, z) : validateChunkData(d0, x, z);
	}
	
	/**
	 * Tries to update this chunk data to the appropriate coords. Fairly cheap if chunkdata object
	 * does not need to change.
	 * 
	 * <b>FORCES UNLOADED CHUNKS</b>.
	 */
	private IChunk forceChunkData(IChunk d0, int x, int z) 
	{
		if (d0.xPos() == x >> 4 && d0.zPos() == z >> 4) return d0;
		IChunk d1 = getChunk(x >> 4, z >> 4);
		if (d1 == null)
		{
			Chunk c = w.getChunkProvider().provideChunk(x >> 4, z >> 4);
			d1 = getOrRegisterChunk(c);
		}
		return d1;
	}
	
	/**
	 * Tries to update this chunk data to the appropriate coords. Fairly cheap if chunkdata object
	 * does not need to change.
	 * 
	 * <b>RETURNS NULL FOR UNLOADED CHUNKS</b>.
	 */
	private IChunk validateChunkData(IChunk d0, int x, int z)
	{
		if (d0 != null && d0.xPos() == x >> 4 && d0.zPos() == z >> 4) return d0;
		return getChunk(x >> 4, z >> 4);
	}

	@Override
	public void setBlock(IChunk d0, int x, int y, int z, Block b, int m, int flags) 
	{
		d0.setBlock(x & 0xF, y, z & 0xF, b, m);
		if ((flags & 1) != 0)
		{
			if (b instanceof BlockFiniteFluid) d0.setUpdate(x & 0xF, y, z & 0xF, true);
			markNeighborFluidUpdates(d0, x, y, z); //update neighboring blocks
		}
		if ((flags & 4) != 0) updateBlock(this.w, x, y, z, b, (flags & 8) != 0);
	}

	
	/** Drops this block as an item (deferred), then sets this block to air (update is marked immediately) */
	@Override
	public void dropBlock(IChunk d0, Block b0, int x, int y, int z, int m)
	{
		new DropBlock(w, x, y, z, b0, m);
		setBlock(d0, x, y, z, Blocks.air, 0, 0xF);
	}
	
	@Override
	public void setFluid(IChunk d0, int x, int y, int z, Block b0, int m0, BlockFiniteFluid f1, int l1, int flags) 
	{
		
		int cx = x & 0xF, cz = z & 0xF;
		
		//we need to know if the block has changed.
		if (b0 == null) b0 = d0.getBlock(cx, y, cz);
		if (m0 < 0) m0 = d0.getMetadata(cx, y, cz);

		//get the details of the new block
		Block b1 = f1.getAppropriateBlockType(l1);
		int m1 = f1.getMetaFromLevel(l1);
		
		d0.setBlock(cx, y, cz, b1, m1); //set it
		d0.setFluidData(cx, y, cz, l1); //update the fluid data arrays
		
		if (l1 < Settings.FLUID_MAX) d0.setPressure(cx, y, cz, 0); //remove pressure if appropriate

		
		//now process all the flags
		if ((flags & 1) != 0) //update fluids
		{
			if (b1 instanceof BlockFiniteFluid) d0.setUpdate(cx, y, cz, true);
			markNeighborFluidUpdates(d0, x, y, z); //update neighboring blocks
		}
		
		//if the block has changed from air, update lighting
		if (b0 == Blocks.air && b1 != Blocks.air && (flags & 2) != 0 && f1.getLightOpacity() > 0)
		{
			int index = (z & 0xF) << 4 | (x & 0xF);

			
			//update the heightmap
			if (d0.getChunk().heightMap[index] < y)
			{
				d0.getChunk().heightMap[index] = y;
				d0.getChunk().isModified = true;
			}
			
			//now flag to update this column if it is lit
			if (d0.getChunk().getSavedLightValue(EnumSkyBlock.Sky, cx, y, cz) > 0 || d0.getChunk().getSavedLightValue(EnumSkyBlock.Block, cx, y, cz) > 0)
			{
				d0.getChunk().updateSkylightColumns[cz << 4 | cz] = true;
				d0.getChunk().isModified = true;
			}

		}
		
		//flags = 0x2 => something about lighting?
		
		if (b0 != b1 || m0 != m1)
		{
			if ((flags & 0x8) != 0) updateBlock(this.w, x, y, z, b1, (flags & 4) != 0);
			else if ((flags & 4) != 0) d0.flagBlockUpdate(cx, y, cz);
		}
	}

	@Override
	public void markNeighborFluidUpdates(IChunk d0, int x, int y, int z) 
	{
		if (y < 255) d0.setUpdateImmediate(x & 0xF, y + 1, z & 0xF, true); //mark the above block for an immediate update
		if (y > 0) d0.setUpdate(x & 0xF, y - 1, z & 0xF, true);
		for (int i = 0; i < 4; i++) //do the four cardinal directions
		{
			final int x1 = x + Util.getDir(i)[0];
			final int z1 = z + Util.getDir(i)[1];
			IChunk d1 = validateChunkData(d0, x1, z1);
			if (d1 != null) d1.setUpdate(x1 & 0xF, y, z1 & 0xF, true);
		}
	}

	@Override
	public void updateBlock(World w, int x, int y, int z, Block b, boolean updateNeighbors) 
	{
		new BlockUpdate(w, x, y, z, b, updateNeighbors);
	}



}
