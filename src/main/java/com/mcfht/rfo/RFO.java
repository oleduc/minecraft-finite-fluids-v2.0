package com.mcfht.rfo;

import java.util.concurrent.atomic.AtomicLong;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.blocks.interactions.InteractionWaterLava;
import com.mcfht.rfo.compatibility.ATGRiverPatch;
import com.mcfht.rfo.compatibility.BCFluids;
import com.mcfht.rfo.compatibility.CompatibilityManager;
import com.mcfht.rfo.compatibility.CompatibilityManager.Stage;
import com.mcfht.rfo.compatibility.GlennsGases;
import com.mcfht.rfo.compatibility.StreamsPatch;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.datastructure.Manager;
import com.mcfht.rfo.rendering.FluidRenderer;
import com.mcfht.rfo.scheduler.ClientServer;
import com.mcfht.rfo.scheduler.FluidUpdater;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.IClientUpdater;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.IFluidUpdater;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.IThreadManager;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.ITicker;
import com.mcfht.rfo.scheduler.deferred.ServerTask;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Observer;
import com.mcfht.rfo.utils.Util;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.DummyModContainer;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.LoadController;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import cpw.mods.fml.relauncher.Side;
import io.netty.util.internal.ConcurrentSet;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;

/* Naming scheme
 * 
 * x/y/z => world x/y/z (or x/y/z in working context)
 * cx, cy, cz => x/y/z & 0xF (aka, chunk/EBS coords)
 * ey => index of ebs array (y >> 4)
 * 
 * x/zPos => position of chunks (x/z >> 4)
 * 
 * c => chunk
 * w/mw/sw/cw => Minecraft/Client/Server World
 * d/C => Chunk Data
 * D => Domain (?)
 * W => World Data
 * 
 * m => metadata
 * b => Block
 * f => BlockFiniteFluid
 * l => fluid level
 * 
 */

/**
 * @author FHT
 */
//@Mod(modid = RFOInfo.MODID, name = "Finite Fluids", version = RFOInfo.VERSION)
public class RFO extends DummyModContainer
{
    
    public RFO()
    {
    	super (new ModMetadata());
    	RFOInfo.get(this.getMetadata());
    }
    
    //called first
    @Override
    public boolean registerBus(EventBus bus, LoadController lc)
    {
    	bus.register(this);
    	return true;
    }
    
    /** Fires when server loads, used to register our commands*/
    @Subscribe
    public void serverStart(FMLServerStartingEvent event)
    {
    	Util.heading("Registring Commands");
		MinecraftServer server = event.getServer();
		if (server != null) ((ServerCommandManager)server.getCommandManager()).registerCommand(new RFOCommands());
		
    }
    
    private int count = 0;
    
    @Subscribe
	public void preInit(FMLPreInitializationEvent event)
	{
    	//register for events
    	FMLCommonHandler.instance().bus().register(this);
    	MinecraftForge.EVENT_BUS.register(Manager.instance);
    	
    	
    	
    	//config
		Settings.configPath = event.getSuggestedConfigurationFile();
		Settings.handleConfigs();
		
		//perform block and TE registration
    	RFOBlocks.init();
		
    	//Now apply some stuff to water and lava that we couldn't apply earlier
    	//if we do this stuff earlier, it creates circular references to constructing objects :D

    	//set the correct block natures
    	RFORegistry.registerFluid("water", (BlockFiniteFluid)Blocks.water, (BlockFiniteFluid)Blocks.flowing_water, Blocks.air);
    	RFORegistry.registerFluid("lava", (BlockFiniteFluid)Blocks.lava, (BlockFiniteFluid)Blocks.flowing_lava, Blocks.air);
    	
    	//and the block interactions
    	RFORegistry.registerFluidInteraction("water", "lava", InteractionWaterLava.instance);
    	RFORegistry.registerFluidInteraction("lava", "water", InteractionWaterLava.instance);

    	//register the first compatibility patches
    	CompatibilityManager.registerCompatibilityPatch(new StreamsPatch(), Stage.POST);
     	CompatibilityManager.registerCompatibilityPatch(new BCFluids(), Stage.PRE);
     	CompatibilityManager.registerCompatibilityPatch(new ATGRiverPatch(), Stage.POST);
     	CompatibilityManager.registerCompatibilityPatch(new GlennsGases(0), Stage.POST);
     	CompatibilityManager.registerCompatibilityPatch(new GlennsGases(1), Stage.POST);

    	//and run any compatibility patches
    	CompatibilityManager.performPatches(Stage.PRE);
    	
	}

	
	
    @Subscribe
    public void init(FMLInitializationEvent e)
    {
    	if (e.getSide() == Side.CLIENT)
    		RenderingRegistry.instance().registerBlockHandler(new FluidRenderer());
    	
    	CompatibilityManager.performPatches(Stage.PERI);

    }
    
    @Subscribe
    public void postInit(FMLPostInitializationEvent e)
    {
    	//Run any code/compatibility patching that needs to occur during post-initialization
    	CompatibilityManager.performPatches(Stage.POST);
    }
    

    
    /////////////////////// NOW WE CAN DO MORE FUN STUFF - TICK EVENTS ETCETERA ///////////////////////
    
    
    /** The random tick system to be performed. */
    private static final ConcurrentSet<ITicker> randomTickers = new ConcurrentSet<ITicker>();
    public static final void registerNewTicker(ITicker t)
    {
    	randomTickers.add(t);
    }
    
    /** Performs random ticks on the provided IChunk. */
    public static void doRandomTicks(IChunk d)
    {
    	for (ITicker t : randomTickers)
    	{
    		t.doRandomTick(d);
    	}
    }
    
    /** The updater to use */
    public static final IFluidUpdater updater = FluidUpdater.instance;
    
    /** The updater to use */
    public static final IThreadManager threadManager = FluidUpdater.instance;
    
    /** The block sender to use */
    public static final IClientUpdater sender = new ClientServer();
    
    /** A universal tick tickCounter. */
    public static /*volatile*/ int tickCounter;
    
	/** The number of update sweeps that have occurred */
	public static long sweepCounter = 0;
	
	public static long startTime;
    
    
    /** This is like, the engine of the mod. Changing any of the stuff here, is NOT a good idea, unless you know
     * exactly what you are doing. */
    @SubscribeEvent
    public void serverTick(ServerTickEvent e)
    {
    	if (e.phase == Phase.START)
    	{
    		tickCounter = (tickCounter + 1) % Integer.MAX_VALUE;
	    	if (Settings.FLOW_ENABLED && tickCounter % Settings.TICK_CURRENT == 0)
	    	{
	    		sweepCounter++;
	    		
	    		startTime = System.currentTimeMillis();
	    		
	    		for (IWorld w : Manager.instance.getWorlds())
	    		{
	    			for (IChunk d : w.getChunks())
	    			{
	    				//increment chunk counter
	    				d.elapseTick();

	    				//do random ticks
	    				doRandomTicks(d);
	    				
	    				//update for all the things that can 'see' the fluids in this chunk
	    				testAllObservers(d);
	    				
	    				//update all the chunks that need updating
	    				if (sender.testChunkForSending(d)) { sender.sendChunk(d); }
	    			}
	    		}	    		
	    		//updater.flagUpdates();
	    		threadManager.runWorkers();
	    	}
    	}
    	
    	//now do all the deferred tasks
    	ServerTask.doDeferredTasks();
    }
    
    /**
     * Tests all of the players, and all of the observers that may be registered within the IWorld instances
     * @param d
     */
    public void testAllObservers(IChunk d)
    {
    	//do all of the players
    	if (d.getIWorld().getWorld().playerEntities != null)
    	{
	    	for (Object _p : d.getIWorld().getWorld().playerEntities)
			{
				EntityPlayer mp = (EntityPlayer)_p;
				updater.flagUpdates(new Coord(((int) mp.posX) >> 4, -1, ((int) mp.posZ) >> 4), d);
			}
    	}
    	
    	//do any other flagged observers
    	if (d.getIWorld().getObservers() != null)
    	{
			for (Observer o : d.getIWorld().getObservers())
			{
				updater.flagUpdates(new Coord(o.c.x >> 4, o.range, o.c.z >> 4), d);
			}
    	}
    }
    
    
    
    /** Notifies the mod that things are running overtime by the given amount, doesn't do anything right now,
     * and probably is not even called anywhere. */
    public void notifyThreadOvertime(long overtime) {}





}
