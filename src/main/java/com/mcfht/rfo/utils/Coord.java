package com.mcfht.rfo.utils;

import java.util.LinkedList;
import java.util.Queue;

import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;

import net.minecraft.world.chunk.Chunk;

/**
 * A simple wrapping class for x/y/z coordinates. This class behaves mostly as
 * a regular tuple, but provides *some* specialized functionality for handling
 * coordinates. <b>This class provides no inherent distinction between world,
 *  chunk, and ebs coordinates.</b>
 * @author FHT
 *
 */
public class Coord {

	public int x, y, z;
	
	/**
	 * Create a new coordinate tuple with arbitrary x/y/z coordinates.
	 * @param X
	 * @param Y
	 * @param Z
	 */
	public Coord(int X, int Y, int Z) {
		x = X; y = Y; z = Z;
	}
	
	public Coord() {
		this(0,0,0);
	}
	
	public Coord(Chunk c) {
		x = c.xPosition; y = c.worldObj.provider.dimensionId; z = c.zPosition;
	}
	
	public Coord(IChunk c) {
		x = c.xPos(); y = c.getIWorld().getWorld().provider.dimensionId; z = c.zPos();
	}
	
	/**
	 * Create this CoordTuple from the encoding described by {@link Util#getIndex12(int, int, int)}.
	 * 
	 * <p>This tuple will be in ebs coordinates (0-15 on all axes)
	 * @param i
	 */
	public Coord(int i)
	{
		x = i & 0xF;
		z = (i >> 4) & 0xF;
		y = (i >> 8) & 0xF;
		
	}
	
	/** Randomizes this coordinate within a 16x16x16 area*/
	public Coord randomize()
	{
		return this.set(Util.rand.nextInt(4096));
	}
	
	/**
	 * Set this CoordTuple from the encoding described by {@link Util#getIndex12(int, int, int)}.
	 * 
	 * <p>This tuple will be in ebs coordinates (0-15 on all axes)
	 * @param i
	 */
	public Coord set(int i)
	{
		x = i & 0xF;
		z = (i >> 4) & 0xF;
		y = (i >> 8) & 0xF;
		return this;
	}

	public Coord add(int dx, int dy, int dz)
	{
		return set(x + dx, y + dy, z + dz);
	}
	
	public Coord set(int x, int y, int z)
	{
		this.x = x; this.y = y; this.z = z; return this;
	}
	
	public Coord clone()
	{
		return new Coord(x, y, z);
	}
	
	
	public String toString()
	{
		return " [" + x + ", " + y + ", " + z + "]";
	}
	
	
	@Override
	public int hashCode()
	{
		return (x) | (y << 11) | (z << 22);
	}
	
	@Override
	public boolean equals(Object o)
	{
		return (o instanceof Coord) && ((Coord)o).x == x && ((Coord)o).y == y && ((Coord)o).z == z;
	}
	
	public enum Direction
	{
		UP,
		DOWN,
		NORTH,
		EAST,
		SOUTH,
		WEST;
		
		public static final Direction get(int val)
		{
			return values()[val % 6];
		}
		
	}
	
	public Coord step(Direction d)
	{
		switch (d)
		{
			case UP: return this.set(x, y+1, z);
			case DOWN: return this.set(x, y-1, z);
			case NORTH: return this.set(x, y, z - 1);
			case SOUTH: return this.set(x, y, z + 1);
			case EAST: return this.set(x + 1, y, z);
			case WEST: return this.set(x - 1, y, z);
			}
		return this;
	}


	
	
}
