package com.mcfht.rfo.utils;

public class Observer 
{
	public Coord c;
	public int range;
	
	public Observer(int x, int y, int z, int range)
	{
		c = new Coord(x,y,z);
		this.range = range;
	}
	
	@Override public boolean equals(Object o) { return c.equals(o); }
	@Override public int hashCode() { return c.hashCode(); }

	
	//maybe more stuff
}
