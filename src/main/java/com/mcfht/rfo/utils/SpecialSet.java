package com.mcfht.rfo.utils;

import java.util.BitSet;

/**
 * This is designed to be used with the Block Packet patches. Basically, a simple set which
 * can be used to remove duplicate block flags.
 * 
 * @author FHT
 */
public class SpecialSet {
	
	BitSet[] BITS = new BitSet[16];
	
	/**
	 * Adds the coordinates to the set.
	 * 
	 * @return True if it was added. For the given usage: true means 'flag the block'
	 * or more precisely, false means 'branch to return'
	 */
	public boolean add(int cx, int y, int cz)
	{
		//System.out.println(" - adding!");
		int i = Util.getIndex12(cx, y & 0xF, cz);
		if (BITS[y >> 4] == null) 
		{
			BITS[y >> 4] = new BitSet(4096);
			BITS[y >> 4].set(i);
			return true;
		}
		boolean flg = BITS[y >> 4].get(i);
		BITS[y >> 4].set(i);
		return flg;
	}
	
	public void clear()
	{
		//System.out.println(" - Clearing!");
		BITS = new BitSet[16];
	}
	
}
