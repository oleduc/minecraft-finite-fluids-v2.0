package com.mcfht.rfo.scheduler;

import java.util.Collection;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Worker;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class UpdaterInterfaces 
{
	
	/**
	 * This interface defines all client<->server block updating behaviors.
	 * 
	 * @author FHT
	 */
	public static interface IClientUpdater
	{
		
		/** Sends all the updates in a chunk to clients */
		public void sendChunk(IChunk d);
		
		/** Processes the chunk. Should be called once every update sweep whether the chunk updates or not.
		 * This method can/should be used to increment the chunk tick tickCounter, update the chunk's distance from
		 * players, etcetera. */
		public void processChunk(IChunk d, int dist, int someParam1, int someParam2);
		

		/** Tests whether the chunk is ready to be sent - is it close enough, and have a sufficient number of
		 * ticks elapsed. For example, near chunks may update every tick, returning true, but far chunks might
		 * update every 5 ticks, so will return false most of the time.
		 * @return Whether this chunk is in range of players */
		public boolean testChunkDist(IChunk d);
		
		/** Tests this chunk to see if it can be sent. Generally should just defer to {@linkplain #testChunkDist(IChunk)}
		 * and {@linkplain IChunk#prepareForSending()}. */
		public boolean testChunkForSending(IChunk d);
		
		/** Flags this entire chunk to receive an update. Chunks marked here, should be processed and updated by
		 * {@linkplain #doAllUpdates()}. */
		public void markChunkDirty(IChunk c);
		
	}

	/**
	 * The parent class for fluid updates. Receives server ticks, and should be responsible for
	 * flagging any chunks that need to be updated.
	 * 
	 * @author FHT
	 */
	public static interface IFluidUpdater
	{
		/** Flags all of the chunks that need to be updated. Should compare distances to
		 * {@linkplain Settings#RANGE_NEAR} and {@linkplain Settings#RANGE_FAR}. This method is called
		 * by the server once every {@linkplain Settings#TICK_CURRENT} ticks. */
		public void flagUpdates(Coord c, IChunk d);
	}
	
	/**
	 * This interfaces describes all functionality for handling Fluid worker threads. 
	 * Implementing classes should be fully and exclusively responsible for their threads.
	 * 
	 * <p>The {@linkplain #runWorkers()} method should run all worker threads, and will be
	 * called automatically in the registered thread manager, once every
	 * {@linkplain Settings#TICK_CURRENT} ticks.
	 * 
	 * @author FHT
	 */
	public static interface IThreadManager
	{
		/** Gets the updates with the given priority (by recommendation: 0 = near, 1 = far). */
		public Collection<IChunk> getUpdates(int priority);
		
		/** Should start worker threads to the appropriate thread count, provide them with tasks to perform,
		 * etcetera. */
		public void runWorkers();
		
		/** Should be called whenever a worker thread completes its work. Needs to be responsible for
		 * making worker threads do more work. */
		public void notifyChunkComplete(FluidWorker w, IChunk c);
		
		/** The number of active threads. */
		public int getActiveThreadCount();
		
		/** All the worker threads being managed by this instance. */
		public Collection<FluidWorker> getWorkers();
		
		/** Notify termination of world */
		public void worldEnding(World w);
		/** Starts running this object again (usually after worldEnding) */
		public void resume();

	}
	
	/** Handles random ticks */
	public static interface ITicker
	{
		/** Perform random ticks within this chunk. There can be more than one random ticker, provided the random tickers
		 * are registered with */
		public void doRandomTick(IChunk c);
		
	}
	
	

	
}
