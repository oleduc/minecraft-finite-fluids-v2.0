package com.mcfht.rfo.scheduler;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.IThreadManager;
import com.mcfht.rfo.utils.Worker;


/**
 * The fluid workers are responsible for flow, pressure, etcetera.
 * 
 * <p>Most synchronization is handled at this level.
 * @author FHT
 *
 */
public class FluidWorker extends Worker
{
	
	IChunk c;
	IThreadManager f;
	
	public FluidWorker(IThreadManager F, IChunk C)
	{		
		super(0);
		c = C; 
		f = F;
	}
	
	/**
	 * Handles the flow update step. This method is very simple, most of the behavior is contained within
	 * {@link Domain#doNextUpdate()}.
	 * <hr>
	 * {@inheritDoc}
	 */
	@Override
	public void doAction()
	{
		c.acquire();
		c.performUpdates();
		c.release();
		f.notifyChunkComplete(this, c);
	}
}