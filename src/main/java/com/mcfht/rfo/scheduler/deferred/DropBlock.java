package com.mcfht.rfo.scheduler.deferred;

import net.minecraft.block.Block;
import net.minecraft.world.World;

/** Drops a block as an item. Assumes that the block is overwritten already. */
public class DropBlock extends ServerTask {

	World w;
	Block b;
	int x, y, z, m;
	
	public DropBlock(World W, int X, int Y, int Z, Block B, int M) {
		
		w = W; 
		b = B; 
		x = X; 
		y = Y; 
		z = Z; 
		m = M;
		
		tasks.add(this);
	}
	
	@Override
	public void perform() 
	{
		b.dropBlockAsItem(w, x, y, z, m, 0);
		w.notifyBlockChange(x, y, z, b);
	}
	
}
