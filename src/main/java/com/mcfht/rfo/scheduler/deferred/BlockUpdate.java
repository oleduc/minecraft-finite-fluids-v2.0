package com.mcfht.rfo.scheduler.deferred;

import net.minecraft.block.Block;
import net.minecraft.world.World;

/** Immediately* updates a block on the clients.
 * 
 * <p>*aka, next server tick.
 * @author FHT
 *
 */
public class BlockUpdate extends ServerTask {

	World w;
	Block b;
	int x, y, z;
	/** Update neighboring blocks. */
	boolean updates;
	
	/** Creates and registers a new block update. */
	public BlockUpdate(World W, int X, int Y, int Z, Block B, boolean Update) 
	{
		
		w = W;
		b = B; 
		x = X; 
		y = Y; 
		z = Z; 
		updates = Update;
		
		tasks.add(this);
	}
	
	@Override
	public void perform() 
	{
		w.markBlockForUpdate(x, y, z);
		if (updates) 
		{
			//u.b.onBlockAdded(u.w, u.x, u.y, u.z);
			w.notifyBlockOfNeighborChange(x, y, z, b);
		}
	}
	
}
