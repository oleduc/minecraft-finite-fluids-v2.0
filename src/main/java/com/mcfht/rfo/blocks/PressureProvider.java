package com.mcfht.rfo.blocks;

import java.util.Random;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.RFORegistry;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.datastructure.Manager;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.tileentities.TileEntityPump;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Util;
import com.mcfht.rfo.utils.Coord.Direction;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityFallingBlock;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.IFluidBlock;

/**
 * 
 * This parent class is used to detect blocks that provide pressure. A pressure provider is expected to
 * provide pressure to neighboring blocks.
 * 
 * <p>The idea of this block is essentially, when something occurs - let's say, a displacement event, this block will
 * simulate some quantity of pressure. Eventually, some other block will reach back to this location, and try to
 * pull some fluid (using {@link #pull(IWorld, IChunk, int, int, int)}).
 * 
 * <p>For a pump-style implementation, this block should adopt a pressure equivalent to the power of the pump. For
 * a displacement style event, this block should adopt a very high pressure value ({@linkplain Settings#PRESSURE_MAX}),
 * and this **should** cause a flow event at a later point.
 * 
 * 
 * @author FHT
 *
 */
public class PressureProvider extends BlockContainer {

	public int pressureMax;
	//public Fluid fluid;
	
	/**
	 * Creates a new pressure provider. The first parameter 
	 * @param f A fluid represented by this block
	 * @param PressureMax The maximum amount of pressure provided by this block
	 */
	public PressureProvider(Material mat, int PressureMax) {
		super(mat);
		pressureMax = PressureMax;
	}

	@Override
	public void breakBlock(World w, int x, int y, int z, Block b0, int m0)
	{
		if (w.isRemote) return;
		
		
		
		//get the right world and chunk objects
		IWorld W = Manager.instance.getOrRegisterWorld(w);
		IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));
		
		Block b1 = d0.getBlock(x & 0xF, y, z & 0xF);
		
		d0.setFluidData(x & 0xF, y, z & 0xF, 0);
		d0.setPressure(x & 0xF, y, z & 0xF, 0);

		//and drop anything that was left in the pump
		TileEntityPump tep = (TileEntityPump)w.getTileEntity(x, y, z);
		if (tep != null && tep.buffer > 0 && tep.f != null && RFORegistry.getFromFluid(tep.f) != null)
			W.setFluid(d0, x, y, z, b0, m0, RFORegistry.getFromFluid(tep.f), Math.min(Settings.FLUID_MAX, tep.buffer), 0x7);
	}
	
	@Override
	public void onBlockAdded(World w, int x, int y, int z)
	{
		if (w.isRemote) return;

		//get the right world and chunk objects
		IWorld W = Manager.instance.getOrRegisterWorld(w);
		IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));
		
		//to be safe
		int m = d0.getMetadata(x & 0xF, y, z & 0xF);
		d0.setFluidData(x & 0xF, y, z & 0xF, 0);
		d0.setPressure(x & 0xF, y, z & 0xF, pressureMax);
		
		//flag us for an update
		d0.setUpdate(x & 0xF, y, z & 0xF, true);
		
	}
	
	@Override
	public void updateTick(World w, int x, int y, int z, Random rand)
	{
		if (w.isRemote) return;
		//System.out.println(" ===> Added <=== ===> [" + x + ", " + y + ", " + z + "] <===");

		//get the right world and chunk objects
		IWorld W = Manager.instance.getOrRegisterWorld(w);
		IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));
		
		//retain our pressure yo
		d0.setPressure(x & 0xF, y, z & 0xF, pressureMax);
		
		//flag us for an update
		d0.setUpdate(x & 0xF, y, z & 0xF, true);
	}
	
	@Override
	public void onNeighborBlockChange(World w, int x, int y, int z, Block b)
	{
		if (w.isRemote) return;
		//System.out.println(" ===> Neighbor Changed <=== ===> [" + x + ", " + y + ", " + z + "] <===");

		//get the right world and chunk objects
		IWorld W = Manager.instance.getOrRegisterWorld(w);
		IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));
		
		d0.setPressure(x & 0xF, y, z & 0xF, pressureMax);
		
		//flag us for an update
		d0.setUpdate(x & 0xF, y, z & 0xF, true);
	}
	
	
	/**
	 * This method should be used to push pressure from provider into environment. This method should be called by
	 * {@link IChunk#performUpdates()} and hence must operate in an unsafe environment.
	 */
	public void updatePressure(IWorld w, IChunk d0, int x0, int y0, int z0, int m0)
	{
		Coord c = new Coord(x0, y0, z0);
		boolean pushed = updateNeighbors(w, d0, x0, y0, z0, m0);
		d0.setPressure(x0 & 0xF, y0, z0 & 0xF, pressureMax);
		if (pushed) d0.setUpdate(c.x & 0xF, c.y, c.z & 0xF, true);
	}
	
	
	
	public boolean updateNeighbors(IWorld w, IChunk d0, int x0, int y0, int z0, int m0)
	{

		
		return false;
	}
	
	
	/** This method is called when something attempts to pull fluid from this block.
	 * 
	 * <p>A general guideline is, this block will either store some quantity of fluid (full blocks of fluid), or
	 * will retrieve some value from a connected tile entity.
	 * 
	 * @return The amount of fluid provided.
	 */
	public int pull(IWorld w, IChunk d0, int x0, int y0, int z0, int amount)
	{
		return 1024;
	}
	


	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) 
	{
		return new TileEntityPump(-1);
	}
	
	/** This method should return how much fluid can be provided by this provider block.
	 * 
	 *  @return the maximum amount of fluid we can pull. 
	 */
	public int tryProvide(IWorld W, IChunk d, int x, int y, int z, Fluid f)
	{
		return 1024;
	}
	
	
}
