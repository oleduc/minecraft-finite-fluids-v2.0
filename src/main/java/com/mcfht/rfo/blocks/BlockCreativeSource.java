package com.mcfht.rfo.blocks;

import java.util.Random;

import com.mcfht.rfo.RFORegistry;
import com.mcfht.rfo.RFORegistry.FFluid;
import com.mcfht.rfo.datastructure.Manager;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Coord.Direction;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

/**
 * The ultimate debugging tool. Informs about neighboring blocks when right clicked, produces infinite fluids when powered,
 * and destroys fluids that touch the top.
 * 
 * <p>This block can only handle the first 16 fluids to be created.
 * @author FHT
 *
 */
public class BlockCreativeSource extends Block
{
	//the texture for this block being disabled.
	public IIcon off;
	
	public BlockCreativeSource(Material mat)
	{
		super(mat);
		this.setTickRandomly(true);
		blockHardness = -1.0F;
		blockResistance = 6000000.0F;
	} 
	
	
	@Override
	public void onNeighborBlockChange(World w, int x, int y, int z, Block b)
	{
		if (w.isBlockIndirectlyGettingPowered(x, y, z) || w.isBlockIndirectlyGettingPowered(x, y + 1, z))
		{
			w.scheduleBlockUpdate(x, y, z, this, 3);			
		}
		

		
	}
	
	@Override
	public void updateTick(World w, int x, int y, int z, Random rand)
	{
		int m = w.getBlockMetadata(x, y, z);

		FFluid ff = RFORegistry.getFFluidByID(m);
		
		if ((ff != null && ff.get(0) != null) 
				&& (w.isBlockIndirectlyGettingPowered(x, y, z) || w.isBlockIndirectlyGettingPowered(x, y + 1, z)))
		{
			w.scheduleBlockUpdate(x, y, z, this, 3);	
			Block b1 = w.getBlock(x, y-1, z);
			
			//now set it if appropriate
			if (b1 == Blocks.air)
			{
				w.setBlock(x, y-1, z, ff.get(0).getBlockFull());
			}
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister reg)
	{
		this.off = reg.registerIcon("bedrock");
	}
		
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int m)
	{
		FFluid f = RFORegistry.getFFluidByID(m);
		
		if (f != null)
		{
			BlockFiniteFluid b = f.get(0);
			if (b != null)
			{
				return b.textures[1];
			}
		}

		return off;
	}
	
	/**
	 * Cycles through the available fluids when right clicked. NOTE: Can only display the first 15 fluids registered.
	 * <hr>{@inheritDoc}
	 */
	@Override
	public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer p, int uK, float px, float py, float pz)
	{
		
		if (!p.isSneaking())
		{
			int m0 = w.getBlockMetadata(x, y, z);
			for (int i = 0; i < (RFORegistry.getRegisteredFluidCount() + 1); i++)
			{
				m0 = ((m0 + 1) % (RFORegistry.getRegisteredFluidCount() + 1)) & 0xF;
				if (m0 == 0 || RFORegistry.getFFluidByID(m0) != null) break;
			}
			
			w.setBlockMetadataWithNotify(x, y, z, m0, 2);
			
			//flag fluid updates on the server :D
			if (w.isRemote == false)
			{
				IWorld W = Manager.instance.getOrRegisterWorld(w);
				IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));
				W.markNeighborFluidUpdates(d0, x, y, z);
			}
			
			w.scheduleBlockUpdate(x, y, z, this, 2);
			
			return true;
		}
		
		return false;
	}
	

}
