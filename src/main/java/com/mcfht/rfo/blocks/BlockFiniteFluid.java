package com.mcfht.rfo.blocks;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.RFOBlocks;
import com.mcfht.rfo.RFORegistry;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.RFORegistry.FFluid;
import com.mcfht.rfo.RFORegistry.FFluid.State;
import com.mcfht.rfo.blocks.interactions.Interaction;
import com.mcfht.rfo.blocks.interactions.InteractionDefault;
import com.mcfht.rfo.datastructure.Manager;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.rendering.FluidRenderer;
import com.mcfht.rfo.scheduler.deferred.BlockIgnition;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Util;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDynamicLiquid;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityFallingBlock;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.EntityWaterMob;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.IFluidBlock;



/**
 * This class handles almost everything to do with liquid mechanics. See docstrings on relevant methods 
 * and constructors for more information. * 
 * @author FHT
 *
 */
public class BlockFiniteFluid extends BlockDynamicLiquid implements IFluidBlock
{
	
	/** The rate at which this block updates. Should access only from: {@link BlockFiniteFluid#getTickRate(World)}.</b>
	 * TODO: Can this use a field in {@linkplain Fluid} */
	public final int tickRate;
	
	/** The internal friction of this fluid. Determines how much to flow. Indicates a minimum fluid level that can transfer
	 * into a new block TODO: Can minBlock use a field in {@linkplain Fluid}? */
	public final int minBlock;
	
	/** The float viscosity of this fluid */
	public final float viscosity;
	
	/** This represents kg per block (where one block is 1000 liters). TODO: can fluid density use a field in {@linkplain Fluid}*/
	public final int density;
	
	/** Name of the texture for still block */
	public String texStill; 
	
	/** Name of the texture for flowing block */
	public String texFlowing;
	
	/** the textures for this fluid*/
	public IIcon textures[] = new IIcon[2];

	
	/** The finite fluid associated with this fluid. */
	public FFluid ffluid;
	
	/** A table of interactions that occur between fluids. Low load factor for higher performance. */
	private Map<Fluid, Interaction> interactions = new HashMap<Fluid, Interaction>(16, 0.33F);
	

	/**
	 * Initialize a new fluid.
	 * 
	 * @param f The {@linkplain FFluid} instance to associate with this fluid.
	 * <p>
	 * @param mat The {@linkplain Material} of this fluid.
	 * <p>
	 * @param visc The viscosity of this fluid. Viscosity determines the speed and fluidity of
	 * flow. See {@link #computeMinBlock(float)} and {@link #computeTickRate(float)} for details. Water
	 * has a viscosity of 1.0F, Lava of 5.0F (2.5F in Nether). Sane values: 0.5F - 5F.
	 * <p>
	 * @param density Density determines the relative weight of a full block of fluid. Heavy fluids displace lighter fluids.
	 * The values should loosely align with the KG weight of a full block (1000L, 265 galleons), although this is not strictly
	 * required. Water generally has a density of 1000, Lava of 3000, Oil of 850. 
	 * <p>
	 * @param tex The qualified string names of the textures to use, as {still, flowing}. For example,
	 * {modID + ":" + tex_still, modID + ":" + tex_flow}.
	 */
	public BlockFiniteFluid(FFluid f, Material mat, float visc, int density, String[] tex) 
	{
		super(mat);
		
		//It is extremely important that this be set. Utterly 100% vital.
		this.ffluid = f;
		
		//Set the stats for the fluid. It is relatively important that this stuff not be changed.
		this.viscosity = visc;
		this.tickRate = computeTickRate(visc);
		this.minBlock = computeMinBlock(visc);
		this.density = density;
		this.texStill = tex[0];
		this.texFlowing = tex[1];

	}
	
	/** Calculates the minimum block content to form during horizontal flow events, using the viscosity as a base.
	 * 
	 * <p>Extremely high viscosity fluids will always have a viscosity less than half a block.
	 * 
	 * <p>near inviscid fluids are insane and discouraged.
	 */
	public static final int computeMinBlock(float visc)
	{
		return (int) Math.max(1, ((0.499F * visc * (float)Settings.FLUID_MAX) / (Math.pow(visc, 1.115) + 4)));
	}
	
	/** Calculates the tick rate for this block. The tick rate will always be 1 or greater.
	 */
	public static final int computeTickRate(float visc)
	{
		return (int)Math.max(1, Math.ceil(Math.pow(visc, 1.25)));
	}
	
	/** Register a material interaction. Interactions registered here are only checked between instances of BlockFiniteFluid.
	 * If no interaction is registered, then the destination fluid will be treated as a solid block.
	 */
	public final void registerInteraction(Fluid f, Interaction interaction) { interactions.put(f, interaction); }
	
	@SideOnly(Side.CLIENT) @Override 
	public void registerBlockIcons(IIconRegister reg)
	{
		super.registerBlockIcons(reg); //just to be safety
		
		//we should only be able to reach here if Buildcraft is actually installed.
		textures[0] = reg.registerIcon(texStill);
		textures[1] = reg.registerIcon(texFlowing);
	}
		
	@SideOnly(Side.CLIENT) @Override 
	public IIcon getIcon(int side, int m)
	{
		 return side > 1 ? textures[1] : textures[0];
	}
	
    /** {@inheritDoc} <hr> Ensure that distinct fluids are rendered as such, even when their materials match. */
    @Override @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess w, int x, int y, int z, int s)
    {
    	Block b = w.getBlock(x, y, z);
    	if (b instanceof BlockFiniteFluid)
		{
    		return ((BlockFiniteFluid)b).ffluid != ffluid;
		}
        return b.getMaterial() == this.blockMaterial ? false : super.shouldSideBeRendered(w, x, y, z, s);
    }
    
    /** {@inheritDoc} <hr> Render fluids in layers rather than stretching different fluids weirdly */
    @Override
    public int getRenderType() {
    	return FluidRenderer.RENDERID;
    }
	
    /** {@inheritDoc} <hr> When borked, we need to either clear our fluid, pressure, etc. */
	@Override
	public void breakBlock(World w, int x, int y, int z, Block b0, int m0)
	{
		if (w.isRemote) return;
		
		//get the correct world and chunk objects
		IWorld W = Manager.instance.getOrRegisterWorld(w);
		IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));
		
		//and the block we have been replaced by
		Block b1 = d0.getBlock(x & 0xF, y, z & 0xF);

		//we don't want to go through some of the stuff here very often - like the filthy dirty stack trace stuff, for example.
		if (b1 != Blocks.air && b1 instanceof BlockFiniteFluid == false)
		{
			//ultimate debugging feature
			if (b1 == Blocks.redstone_torch)
			{
				int _d = d0.getFluidData(x & 0xF, y, z & 0xF);
				Util.enterSection("Debugging a fluid block at " + new Coord(x, y, z), 6);
				Util.info(" ");
				Util.info("Block: " + b0.getUnlocalizedName() + " (id: " + getIdFromBlock(b0) + ", m: " + m0 + ")");
				Util.info("Density: " + density + ", viscosity: " + viscosity + "(" + getTickRate(w) + ", " + getMinBlock(w) + ")");
				Util.info("Associated Fluid: " + this.getFluid().getName() + " (RFO ID: " + this.ffluid.ID + "). Is full variant: " + (this == getBlockFull()));
				Util.info("Fluid data: " + d0.getFluid(b0, m0, x & 0xF, y, z & 0xF) + " (stored: " + _d + "), Pressure: " + d0.getPressure(x & 0xF, y, z & 0xF));
				Util.info(" ");
				Util.leaveSection();
				
				//Make sure players don't randomly lose redstone torches
				b1.dropBlockAsItem(W.getWorld(), x,  y,  z, 0, 0);
				
				//but make it not erase the fluid so that we can debug better
				W.setBlock(d0, x, y, z, b0, m0, 0x7);
			}
			else
			{
				// Extreme hacks? 
				// I mean, nothing should get freaky between here and piston extension for example... probably...
				final StackTraceElement[] stack = Thread.currentThread().getStackTrace();
				if (b1 == Blocks.piston_extension
					|| (b1 instanceof BlockFalling && stack[4].getClassName().equals(EntityFallingBlock.class.getName()))
					|| (stack[5].getClassName().equals(BlockPistonBase.class.getName())))
					//we should try to displace this fluid :D
					displace(d0, d0.getFluid(this, -1, x & 0xF, y, z & 0xF), x, y, z, 32, true, true);
				
				else
				{
					//otherwise, there are no special behaviors, and it's not a fluid
					//so we need to erase the fluid and pressure that was stored here.
					d0.setFluidData(x & 0xF, y, z & 0xF, 0);
					d0.setPressure(x & 0xF, y, z & 0xF, 0);
				}
			}
		}
	}
	
    /** {@inheritDoc} <hr> When added, we need to make sure we are the correct block type, have stored fluid, etc. */
	@Override
	public void onBlockAdded(World w, int x, int y, int z)
	{
		if (w.isRemote) return;
		//System.out.println(" ===> Added <=== ===> [" + x + ", " + y + ", " + z + "] <===");
		
		//get the right world and chunk objects
		IWorld W = Manager.instance.getOrRegisterWorld(w);
		IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));

		int m = d0.getMetadata(x & 0xF, y, z & 0xF);
		int l = this == getBlockFull() ? Settings.FLUID_MAX : getLevelFromMeta(m);
		Block b = getAppropriateBlockType(l);

		//set info about the block. This ensures that we have the right block and metadata.
		d0.setFluidData(x & 0xF, y, z & 0xF, l);
		d0.setBlock(x & 0xF, y, z & 0xF, b, m);
		d0.setUpdate(x & 0xF, y, z & 0xF, true);
		
	}
	
    /** {@inheritDoc} <hr> */
	@Override
	public void updateTick(World w, int x, int y, int z, Random rand)
	{
		if (w.isRemote) return;
		//System.out.println(" ===> Added <=== ===> [" + x + ", " + y + ", " + z + "] <===");

		//get the right world and chunk objects
		IWorld W = Manager.instance.getOrRegisterWorld(w);
		IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));
		
		
		//ensure the right fluid data is here
		d0.getFluid(this, -1, x & 0xF, y, z & 0xF);
		//flag us for an update
		d0.setUpdate(x & 0xF, y, z & 0xF, true);
		
	}
	
    /** {@inheritDoc} <hr> */	@Override
	public void onNeighborBlockChange(World w, int x, int y, int z, Block b)
	{
		if (w.isRemote) return;

		//get the right world and chunk objects
		IWorld W = Manager.instance.getOrRegisterWorld(w);
		IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));

		//ensure the right fluid data and flag us for an update
		int l = d0.getFluid(this, -1, x & 0xF, y, z & 0xF);
		d0.setUpdate(x & 0xF, y, z & 0xF, true);
		
		//make flammable fluids burn
		if (b == Blocks.fire && ffluid.isFlammable) new BlockIgnition(w, x, y, z, ffluid.flammability, l);

	}
	
	/**
	 * This method causes flow update. Flow algorithm;
	 * 
	 * <li>1. Flow downwards as much as possible
	 * <li>2. Flow horizontally, equalize neighboring blocks (eliminates ripples)
	 * 
	 * <hr>
	 * <p>The pressure heuristic is a bit more complicated.</p>
	 * <li>1. Easy, as we go through steps above, get the highest pressure provided to us
	 * <li>2. If we are under pressure and have a space above, try to flow up.
	 * <li>3. If we lose pressure, invalidate this entire area. </li>
	 * Removing pressure is a pain in the ass. I can't think of a better way to clear it that is not
	 * absurdly expensive when applying to larger volumes of fluid.
	 */
	public void updateFlow(IWorld w, IChunk d0, int x0, int y0, int z0, int m0)
	{
		//System.out.println(" ===> Updating Flow <=== ===> [" + x0 + ", " + y0 + ", " + z0 + "] <===");
		//init some temp variables
		int cx = x0 & 0xF, cy = y0 & 0xF, cz = z0 & 0xF;
		
		int y1, l0, _l0, l1 = 0;
		int _p0 = 0, p0 = 0, pM = 0;
		
		Block b1, bA = null; //bB = block below, bA = block above

		
		//get our current fluid data, store it in l0, and hold it in _l0 for later
		_l0 = l0 = d0.getFluid(this, m0, cx, y0, cz);

		//if we can have pressure, get our pressure as well :D
		if (l0 >= Settings.FLUID_MAX) p0 = _p0 = d0.getPressure(cx, y0, cz);
		
		try
		{
			//clear this block stuff if we are going to fall from the world
			if (y0 < 1)
			{
				if (w.canVoidDrainFluids()) l0 = 0;
				return;
			}

			
			////////////////DOWNWARDS FLOW STEP
			d0.getEbs((y1 = y0 - 1) >> 4, true);

			//make sure there is an ebs here
			Block bB = d0.getBlock(cx, y1, cz); //we can be unsafe here
			int m1 = d0.getMetadata(cx, y1, cz);
			if (isSameFluid(bB)) l1 = d0.getFluid(this, m1, cx, y1, cz); //get our fluid volume if we are a fluid
			else if (bB instanceof PressureProvider) l1 = Settings.FLUID_MAX;
			
			if (canDrain(bB, m1, l0) && (l0 = 0) == 0) return;
			if (l1 < Settings.FLUID_MAX)
			{
				
				int result = testFlow(d0, l0, x0, y0, z0, d0, x0, y1, z0, bB, m1, l1, true);
				if (result == -1) { l0 = -1; return; }
				
				//try to flow through blocks vertically, cbf with horizontal flow
				if (result == 2) 
				{
					//fall from the world
					if ((y1 -= 1) < 1)
					{
						if (w.canVoidDrainFluids()) l0 = 0;
						return;
					}
					//make sure there is an ebs here
					d0.getEbs((y1) >> 4, true);
					bB = d0.getBlock(cx, y1, cz); //we can be unsafe here
					m1 = d0.getMetadata(cx, y1, cz);
					if (isSameFluid(bB)) l1 = d0.getFluid(bB, m1, cx, y1, cz); //get our fluid volume if we are a fluid
					result = testFlow(d0, l0, x0, y0, z0, d0, x0, y1, z0, bB, m1, l1, false);
					
				}
				
				if (result == 1) //we can flow
				{
					//clear pressure
					pM = -1;
					
					int s = l1 + l0; //get the sum
					l1 = s >= Settings.FLUID_MAX ? Settings.FLUID_MAX : s; //cap l1 to be full
					l0 = Math.max(0, s - Settings.FLUID_MAX); //now put the remainder in l0
					w.setFluid(d0, x0, y1, z0, bB, m1, this, l1, 0x7); //update l1			
				}
			}
			else if (d0.isPressureValid())
			{
				//the block below us is full, see what pressure it provides
				pM = Math.max(pM, d0.getPressure(cx, y1, cz) - Settings.PRESSURE_STEP - 1);
			}
			
			//if we are now empty
			if (l0 <= 0) return;

			//make slower fluids flow slower. This way, they fall fast, and can be pushed by fluids on top
			if ((RFO.sweepCounter % getTickRate(w.getWorld())) != 0)
			{
				if (RFO.sweepCounter % Math.max(1, this.getTickRate(w.getWorld()) >> 1) == 0)
				{
					//if we have a lot of fluid on top
					int aboveCount = 0;
					for (int i = 0; i < 3; i++)
					{
						if (y0 + i > 256 || !isSameFluid(d0.getBlock(cx, y0 + i, cz)))
							break;
						aboveCount++;
					}
					
					if (aboveCount >= 3)
					{
						int visc = getMinBlock(w.getWorld());
						//if we are on a big stack, try to fall to the side;
						int dir = Util.randomDirVal4();
						for (int i = 0; i < 4 && l0 > (visc << 1); i++)
						{
							//calculate destination x/z then get the chunk data
							int dx, x1 = x0 + (dx = Util.getDir(i + dir)[0]);
							int dz, z1 = z0 + (dz = Util.getDir(i + dir)[1]);
							IChunk d1 = w.validateChunkData(d0, x1, z1, false);
							
							//don't flow into unloaded chunks
							if (d1 == null) continue;
							d1.getEbs(y0 >> 4, true);
							
							if (d1.getBlock(x1 & 0xF, y0, z1 & 0xF) == Blocks.air)
							{
								int toMove = l0 >> 1;
								if (toMove > visc && l0 - toMove > visc)
								{
									l0 -= toMove;
									w.setFluid(d1, x1, y0, z1, Blocks.air, 0, this, toMove, 0x7);
								}
								break;
							}
						}
					}
				}
				
				d0.setUpdate(cx, y0, cz, true);
				return;
			}

			////////////////HORIZONTAL FLOW STEP
			
			//calculate an effective minBlock value for this fluid
			//this step allows us to make two stacked blocks of fluid, behave slightly more like a single entity
			//int eVisc = isSameFluid(bB) ? getMinBlock(w.getWorld()) >> 3 : getMinBlock(w.getWorld());
			int eEq = Settings.EQUALIZE_NATURAL ? getEquilibriumRequirement(w.getWorld(), bB) : Integer.MAX_VALUE;
			
			int dir = Util.randomDirVal4();
			for (int i = 0; i < 4; i++)
			{
				//calculate destination x/z then get the chunk data
				int dx, x1 = x0 + (dx = Util.getDir(i + dir)[0]);
				int dz, z1 = z0 + (dz = Util.getDir(i + dir)[1]);
				IChunk d1 = w.validateChunkData(d0, x1, z1, false);
				
				//don't flow into unloaded chunks
				if (d1 == null) continue;
				
				//make sure the EBS is present
				d1.getEbs(y0 >> 4, true);

				l1 = 0;
				b1 = d1.getBlock(x1 & 0xF, y0, z1 & 0xF);
				m1 = d0.getMetadata(x1 & 0xF, y0, z1 & 0xF);
				if (isSameFluid(b1)) l1 = d1.getFluid(b1, m1, x1 & 0xF, y0, z1 & 0xF); //get our fluid volume if we are a fluid
				else if (b1 instanceof PressureProvider) l1 = Settings.FLUID_MAX;
				
				if (l1 < Settings.FLUID_MAX)
				{
					//if (b1 == Blocks.air || l1 > 0) pM = -1;
					if (l1 < l0)
					{
						//see what happens if we try and flow
						int result = testFlow(d0, l0, x0, y0, z0, d1, x1, y0, z1, b1, m1, l1, false);
						if (result == -1) { l0 = -1; return; }
						
						if (result == 1) //we can flow
						{
							//we flowed. Lose pressure!
							pM = -1;
							int visc = getMinBlock(w.getWorld());
							//This allows larger volumes of fluid to level out over time
							//it may also allow large bodies of fluid (rivers, oceans) to become functionally infinite if used sparingly.
							if (l0 >= eEq && l1 >= visc) l0 = l1 = (l0 + l1 + 1) >> 1;
							else
							{
								int toMove = (l0 - l1) >> 1;
								if (l1 + toMove >= visc)
								{
									l0 -= toMove;
									l1 += toMove;
								}
								//otherwise if we are on a ledge
								else if (isSameFluid(bB) == false)
								{
									//calculate l1, calculate l1B
									if ((y1 = y0 - 1) < 0)
									{
										if (w.canVoidDrainFluids()) l0 = 0;
										return;
									}
									Block b1B = d1.getBlock(x1 & 0xF, y1, z1 & 0xF);
									int l1B = 0;
									if (isSameFluid(b1B)) l1B = d1.getFluid(b1B, -1, x1 & 0xF, y1, z1 & 0xF);
									
									//now try to move down into b1B
									if (canAcceptFlow(b1B) && l1B < Settings.FLUID_MAX)
									{
										
										boolean pulled = false;
										//this flow type is relatively rare, so we can probably do this;
										
										//grab the block in the opposite direction of our current flow
										//this will pull most of the fluid, but sometimes leave patches, which is
										//kind of neato
										int xN = x0 + Util.getDir((i + dir) ^ 0x1)[0];
										int zN = z0 + Util.getDir((i + dir) ^ 0x1)[1];
										IChunk dN = w.validateChunkData(d1, xN, zN, false);
										
										//if we are safe to grab from this location
										if (dN != null && dN.getEbs(y0 >> 4, false) != null) 
										{
											//just pull all the content
											int lN = 0;
											Block bN = dN.getBlock(xN & 0xF, y0, zN & 0xF);
											
											int mN = dN.getMetadata(xN & 0xF, y0, zN & 0xF);
											if (isSameFluid(bN)) lN = dN.getFluid(bN, mN, xN & 0xF, y0, zN & 0xF); //get our fluid volume if we are a fluid
											
											//if we have fluid, and we won't overshoot our viscosity allowance
											if (lN > 0 && l1 + lN < (visc << 1))
											{
												l1 += lN;
												w.setFluid(dN, xN, y0, zN, bN, 0, this, 0, 0x7);
												pulled = true;
											}
										}
										
										//otherwise, just move ourselves over the edge instead
										if (pulled == false)
										{
											l1 += l0;
											l0 = 0;
										}
									}
								}
							}
							//update the flow destination
							w.setFluid(d1, x1, y0, z1, b1, d1.getMetadata(x1 & 0xF, y0, z1 & 0xF), this, l1, 0x7);	//update l1			
						}
					}
				}
				//else, if pressure is allowed, see if this block provides us with pressure
				else if (d1.isPressureValid() && pM >= 0 )
				{
					pM = Math.max(pM, d1.getPressure(x1 & 0xF, y0, z1 & 0xF) - 1);
				}
			}

			//Next we will do pressure
			//exit early if that will not be allowed to happen
			if (l0 < Settings.FLUID_MAX || d0.isPressureValid() == false || y0 >= 255) return; 

			//////////////Pressurized flow events //////////////

			//check from above
			int lA = 0;
			d0.getEbs((y1 = y0 + 1) >> 4, true);
			bA = d0.getBlock(cx, y1, cz); 
			
			//if the same fluid, or a pressure provider
			if (isSameFluid(bA)) lA = d0.getFluid(this, -1, cx, y1, cz);
			else if (bA instanceof PressureProvider) l1 = Settings.FLUID_MAX;
			
			//the highest pressure allowed
			if (lA >= Settings.FLUID_MAX) pM = Math.max(pM, d0.getPressure(cx, y1, cz) + Settings.PRESSURE_STEP - 1);
				
			pM = Math.min(Settings.PRESSURE_MAX, pM);
			if (l0 >= Settings.FLUID_MAX && pM >= p0) p0 = pM;
			else p0 = 0;
			
			if (p0 != _p0)
			{
				//System.out.println(y0 + " ==>> Updating pressure from " + _p0 + " to " + p0);
				d0.setPressure(cx, y0, cz, p0);
				d0.setUpdate(cx, y0, cz, true);
				d0.getIWorld().markNeighborFluidUpdates(d0, x0, y0, z0); //update neighboring blocks
				if (p0 < _p0) { d0.invalidatePressure(); return; }
			}

			// Try to flow up.
			//sledgehammer. Reduce the upper bound slightly
			//should reduce pressure calcs along edges between full and non-full blocks.
			if (p0 >= Settings.PRESSURE_STEP && lA < Settings.FLUID_MAX && canAcceptFlow(bA))
			{

				int[] res = moveToHighPressure(w, d0, x0, y0, z0, l0, p0);
				IChunk dN = w.validateChunkData(d0, res[0],  res[2], false);

				Block bN = dN.getBlock(res[0] & 0xF,  res[1], res[2] & 0xF);
				
				//if the target block is a pump or something like this
				if (bN instanceof PressureProvider)
				{
					
					PressureProvider pp = (PressureProvider)bN;
					
					//get the max amount that we can move into the block above
					int toMove = Settings.FLUID_MAX - lA;
					
					//now get the max amount that we can move from the provider
					int lM = pp.tryProvide(dN.getIWorld(), dN, res[0], res[1], res[2], this.ffluid.fluid);
					if (lM <= 0) return;
					
					//now pull as much as we can until the block above is full
					pp.pull(dN.getIWorld(), dN, res[0], res[1], res[2], toMove);
					
					//assumption: if toMove fills the block, but the provider does not provide sufficient fluid
					//then we simply emptied the provider
					lA += lM;
					
					if (lA > Settings.FLUID_MAX && y1 < 255)
					{
						//get the block one higher again
						Block bAA = d0.getBlock(cx, y1 + 1, cz);
						if (canAcceptFlow(bAA))
						{
							int lAA = bAA == Blocks.air ? 0 : d0.getFluid(bAA, -1, cx, y1 + 1, cz);
							
							//assumption: never pulls more than a full block
							lAA = lA - Settings.FLUID_MAX;
							
							//now drain the provider for the rest of the allowable quota;
							pp.pull(dN.getIWorld(), dN, res[0], res[1], res[2], lA - Settings.FLUID_MAX);
							
							//and set the block
							w.setFluid(d0, x0, y1 + 1, z0, bAA, -1, this, lAA, 0x7);
						}
					}
					
					w.setFluid(d0, x0, y1, z0, bA, -1, this, Math.min(Settings.FLUID_MAX, lA), 0x7);

					//TODO: Do an action at this pressure provider to notify that fluid was taken?
					
					dN.invalidatePressure(); d0.invalidatePressure();
				}
				else if (res[3] > 2) //it's not valid to have pressure flow from less than 3 blocks away
				{
					
					int lN = Settings.FLUID_MAX;
					int toMove = Settings.FLUID_MAX - lA;
					if (toMove > 0)
					{
						d0.getIWorld().setFluid(dN, res[0], res[1], res[2], this, -1, this, Settings.FLUID_MAX - toMove, 0x7);
						d0.getIWorld().setFluid(d0, x0, y1, z0, Blocks.air, 0, this, lA + toMove, 0x7);
					}

					dN.invalidatePressure(); d0.invalidatePressure();
					
				}
			}
			
			
		}
		finally
		{
			if (l0 != _l0 && l0 >= 0) //we changed in volume AND not flagged to immediately END all processing.
			{
				w.setFluid(d0, x0, y0, z0, this, m0, this, l0, 0x7); //so update ourselves.
			}
		}

	}
	
	/**
	 * Finds the highest pressure block connected to x0, y0, z0. The pressure heuristic implies that the destination reached
	 * by this method, is the nearest pressure provider within the tested distance.
	 * 
	 * @return {x, y, z, number_of_steps}
	 */
	public int[] moveToHighPressure(IWorld w, IChunk d0, int x0, int y0, int z0, int l0, int p0)
	{
		int pN = p0, pM = p0;
		int xN, yN, zN, xM = xN = x0, yM = yN = y0, zM = zN = z0;
		IChunk dN = d0;
		Block bN;
		
		int nDir = -1;
		int dist = 0;
		
		//System.out.println(" ===> Searching for pressure from: " + cStr(x0, y0, z0) + ", Level: " + l0);
		//move lots of blocks - we're using multiple threads so who really gives a damn lol
		//(but seriously, we can only do about one of these per sweep per chunk anyway).
		for (int step = 0; step < 64; step++)
		{
			//exit early if we've gone up a really long way. This will slightly reduce the amount of work done,
			//for example when deep caves get flooded.
			if (yM > y0 + 16) break; 

			int highDir = -1, dy = 0;
			//System.out.println(" - finding neighbors at: " + cStr(xM, yM, zM));

			for (int i = 0; i < 6; i++)
			{
				if (i == nDir) continue; //we came from this direction, don't bother going back
				xN = xM + Util.getFace(i)[0];
				yN = yM + (dy = Util.getFace(i)[1]);
				zN = zM + Util.getFace(i)[2];
				
				//don't bother if we land in an unloaded or invalidated chunk
				if ((dN = w.validateChunkData(dN, xN, zN, false)) == null || dN.isPressureValid() == false)
						break;
				
				//don't bother traveling into unsafe ebs arrays
				if (dN.getEbs(yN >> 4, false) == null) break;
				bN = dN.getBlock(xN & 0xF, yN, zN & 0xF);

				//don't bother checking non-fluids
				if (isSameFluid(bN) == false)
				{
					if (bN instanceof PressureProvider)
					{
						pN = dN.getPressure(xN & 0xF, yN, zN & 0xF) + (dy * Settings.PRESSURE_STEP);
						if (pN > pM) return new int[] { xN, yN, zN, dist };
					}
					
					continue;
				}
				int lN = dN.getFluid(this, -1, xN & 0xF, yN, zN & 0xF);
				
				//if this block can sustain pressure, get the applied pressure
				if (lN < Settings.FLUID_MAX) continue; 
				pN = dN.getPressure(xN & 0xF, yN, zN & 0xF);
				//if it provides more than the MOST, then store it
				if (pN + (dy * Settings.PRESSURE_STEP) > pM)
				{
					highDir = i;
					pM = pN;
				}
			}
			//now get the highest applied pressure from above and now repeat the above steps from this destination.
			if (highDir >= 0)
			{
				nDir = highDir ^ 0x1; //flip the direction
				dist += 1;
				//move to the new highest
				xM = xM + Util.getFace(highDir)[0];
				yM = yM + Util.getFace(highDir)[1];
				zM = zM + Util.getFace(highDir)[2];
				//System.out.println(" ** New highest: " + cStr(xM, yM, zM));

			}
			else break;
		}
		return new int[]{xM, yM, zM, dist};
	}
	
	

	
	/**
	 * Attempts to displace this fluid into different places. Moves upwards in a column, pushing water upwards
	 * and to the sides.
	 * @param dist the distance to push the fluid.
	 * @return whether this fluid is destroyed.
	 */
	public boolean displace(IChunk d0, int l0, int x0, int y0, int z0, int dist, boolean clearBlock, boolean toSideFirst)
	{
		//first, to sides
		int cx = x0 & 0xF, cz = z0 & 0xF;
		
		int start = (toSideFirst) ? 1 : 0;
		
		for (int k = start; k < dist && l0 > 0 && y0 < (255 - k); k++)
		{
			//get a random direction
			int dir = Util.randomDirVal4();
			int y1 = y0 + k; //update y
			
			//try to put fluid in the colum, but only if we are above.
			if (y1 > y0)
			{
				int l1 = 0;
				Block b1 = d0.getBlock(cx, y1, cz);
				int m1 = d0.getMetadata(cx, y1, cz);
				
				if (canFlowThroughVertically(b1, m1))
				{
					//move up an extra block
					k++; y1++;
					b1 = d0.getBlock(cx, y1, cz);
					m1 = d0.getMetadata(cx, y1, cz);
				}

				if (isSameFluid(b1)) l1 = d0.getFluid(b1, m1, cx, y1, cz); //get our fluid volume if we are a fluid
				
				//move all the fluid we can into this block
				if (canAcceptFlow(b1))
				{
					if (l1 < Settings.FLUID_MAX)
					{
						int s = l1 + l0; //get the sum
						l1 = s >= Settings.FLUID_MAX ? Settings.FLUID_MAX : s; //cap l1 to be full
						l0 = Math.max(0, s - Settings.FLUID_MAX); //now put the remainder in l0
						d0.getIWorld().setFluid(d0, x0, y1, z0, b1, m1, this, l1, 0x7); //update l1			
					}
				}
				//we can't go any higher
				else break;
			}
			
			//try to put fluid into the 4 directions around us
			for (int i = 0; i < 4 && l0 > 0; i++)
			{
				int x1 = x0 + Util.getDir(i + dir)[0];
				int z1 = z0 + Util.getDir(i + dir)[1];
				IChunk d1 = d0.getIWorld().validateChunkData(d0, x1, z1, false);
				
				//don't flow into crap places
				if (d1 == null) continue;
				if (d1.getEbs(y0 >> 4, true) == null) continue;
				
				//try to get the level of this block
				int l1 = 0;
				Block b1 = d1.getBlock(x1 & 0xF, y1, z1 & 0xF);
				int m1 = d1.getMetadata(x1 & 0xF, y1, z1 & 0xF);
				if (isSameFluid(b1)) l1 = d1.getFluid(b1, m1, x1 & 0xF, y1, z1 & 0xF); //get our fluid volume if we are a fluid
			
				//move all the fluid we can into this block
				if (l1 < Settings.FLUID_MAX && canAcceptFlow(b1))
				{
					int s = l1 + l0; //get the sum
					l1 = s >= Settings.FLUID_MAX ? Settings.FLUID_MAX : s; //cap l1 to be full
					l0 = Math.max(0, s - Settings.FLUID_MAX); //now put the remainder in l0
					d1.getIWorld().setFluid(d1, x1, y1, z1, b1, m1, this, l1, 0x7); //update l1			
				}
			}
			
		}
		
		//and reset the data of this block
		if (clearBlock)
		{
			l0 = 0;
			d0.setFluidData(cx, y0, cz, 0);
			d0.setPressure(cx, y0, cz, 0);
		}
		if (l0 <= 0) return true;
		return false;
		
	}
	


	public static Class<?> classBlockRiver = null;
	/** Whether the given block can drain a fluid from a vertical flow. */
	public boolean canDrain(Block bB, int m1, int l0)
	{
		//let the creative source block drain fluids
		//if it's set to default (looks like bedrock)
		//or of the metadata lines up with our fluid ID (implied: meta of creative block is a fluid ID).
		if (bB == RFOBlocks.creativeSource && (m1 == 0 || ffluid.ID == m1))  return true;

		//allow streams BlockRiver to erase partial blocks. Added by request
		if (classBlockRiver != null && l0 < Settings.FLUID_LAYER && classBlockRiver.isInstance(bB)) return true;
		
		return false;
	}
	
	
	
	/** Attempts to flow into the provided block. Performs all interactions, such as water turning lava to stone,
	 * fluids breaking plants, etc. Override this method to determine custom interaction behaviors.
	 * 
	 * 
	 * @return There are 4 key returns for this method;
	 * <li>0 => No flow
	 * <li>1 => Flow into this block
	 * <li>2 => Can flow 1 extra block in this dir
	 * <li>-1 => This block is not to be processed any further!
	 */
	public int testFlow(IChunk d0, int l0, int x0, int y0, int z0, IChunk d1, int x1, int y1, int z1, Block b1, int m1, int l1, boolean skips)
	{
		//it's a simple air/fluid block
		if (canAcceptFlow(b1)) return l1 >= Settings.FLUID_MAX ? 0 : 1; //only flow into not-full blocks
		
		int dy = y1 - y0;
		if (dy < 0 && canFlowThroughVertically(b1, m1)) return skips ? 2 : 0;
		
		//try to interact with finite fluid
		if (b1 instanceof BlockFiniteFluid)
		{
			//get the correct fluid level
			l1 = d1.getFluid(b1, m1, x1 & 0xF, y1, z1 & 0xF);
			Interaction r = interactions.get(((BlockFiniteFluid)b1).getFluid());
			
			int result;
			if (r == null) r = InteractionDefault.instance;
			result = r.doInteraction(d0, this, l0, x0, y0, z0, d1, (BlockFiniteFluid)b1, l1, x1, y1, z1);
			return result;
		}
		
		//custom non-overrides - i.e, other fluids that are not finite-altered.
		else if (isCustomNonOverwrite(b1)) return 0;
		
		if (ffluid.isFlammable && b1 == Blocks.fire)
		{
			new BlockIgnition(d0.getIWorld().getWorld(), x0, y0, z0, ffluid.flammability, l0);
			return 0;
		}
		
		
		//or try to break the block
		if ((dy < 0 || l0 > (this.getMinBlock(d0.getIWorld().getWorld())) << 1) && canBreak(b1, dy, l0))
		{
			return doBreak(d1, b1, x1, y1, z1, m1);
		}
		
		//We've reached the end, no flow could be found, so return 0.
		return 0;
	}
	
	/** Whether the destination block can accept a flow event from this fluid */
	public boolean canAcceptFlow(Block b1) { return (b1 == Blocks.air || isSameFluid(b1)); }
	
	/** Whether the destination block has custom non-flow behaviors. By default, stops flowing into any liquid block */
	public boolean isCustomNonOverwrite(Block b1)
	{
		return (b1 instanceof BlockLiquid || b1 instanceof IFluidBlock);
	}
	
	/** Returns whether a block can flow through this space vertically. */
	public boolean canFlowThroughVertically(Block b1, int m1)
	{
		return 	   b1 == Blocks.fence 
				|| b1 == Blocks.iron_bars 
				|| b1 == Blocks.nether_brick_fence 
				||(b1 == Blocks.trapdoor && (m1 & 0x4) != 0);
	}
	
	/** Whether the given block can be broken by this fluid */
	public boolean canBreak(Block b1, int dy, int l0) 
	{ 
		return 	   b1.getMaterial().blocksMovement() == false
				&& b1.getMaterial() != this.blockMaterial
				&& b1 != Blocks.wooden_door 
				&& b1 != Blocks.iron_door 
				&& b1 != Blocks.portal; 
	}
	
	/** Allows custom behavior to be added. I.e, lava uses this to burn things rather than drop them as items. */
	public int doBreak(IChunk d1, Block b1, int x1, int y1, int z1, int m1)
	{
		d1.getIWorld().dropBlock(d1, b1, x1, y1, z1, m1);
		return 1;
	}
	
	/** The minimum fluid level required to perform natural equalization steps on this block. */
	public int getEquilibriumRequirement(World w, Block bB)
	{
		return Settings.EQUALIZE_NATURAL_MIN_BLOCK;
	}
	
	
	/////////////////////////////// END OF FLOW STUFF ////////////////////////////////
	//////////////////////////// NOW WE NEED TO OVERRIDE SOME OTHER BLOCK BEHAVIORS ///////////////////////////
	
	
	@Override
	public void velocityToAddToEntity(final World w, final int x, int y, final int z, final Entity e, final Vec3 vec)
	{

		//flammable fluids may combust if they come into contact with fire-y stuff
		if (ffluid.isFlammable && e.isDead == false)
		{
			//burning players/mobs/items
			if (e.isBurning())
			{
				new BlockIgnition(w, x, y, z, ffluid.flammability, Settings.FLUID_MAX >> 2);
			}
			//or if it's a burney item, like a torch or a bucket of lava
			//note: fluids can break torches off walls, which makes this kind of fun :D
			else if ((e instanceof EntityItem))
			{
				Item it = ((EntityItem)e).getEntityItem().getItem();
				if (it == Items.blaze_powder || it == Items.lava_bucket || it == Items.blaze_rod || it == Blocks.torch.getItem(w, x, y, z))
				{
					new BlockIgnition(w, x, y, z, ffluid.flammability, Settings.FLUID_MAX >> 2);
					e.setDead();
				}
			}
		}
		
		// Copy the flow of the above blocks
		final Chunk c = w.getChunkFromChunkCoords(x >> 4, z >> 4);
		
		//allow water to have a current beneath the surface
		for (int i = 0; i < 6 && isSameFluid(w.getBlock(x, y + 1, z)); i++)
			y++;
		
		//now just use the velocity from the higher block :D
		super.velocityToAddToEntity(w, x, y, z, e, vec);
	}

	/** Whether the provided block is the same fluid. Override this method to determine custom behaviors. */
	public boolean isSameFluid(Block b1) { return b1 instanceof BlockFiniteFluid && ffluid.ID == ((BlockFiniteFluid)b1).ffluid.ID; }
	
	/** Returns the minBlock of this fluid. Can be overridden to specify custom behavior, i.e, lava in nether. */
	public int getMinBlock(World w) { return minBlock; }
	
	/** Should return the appropriate block type based on the level of the block (full/partial) */
	public Block getAppropriateBlockType(int l) { return l >= Settings.FLUID_MAX ? getBlockFull() : l <= 0 ? getBlockEmpty() : getBlockPartial(); }
	
	/** Should return the appropriate block for empty blocks (i.e, a block may want to leave something behind, idk why, but yeah.) */
	public Block getBlockEmpty() { return ffluid.empty; }
	
	/** Should return block to use when this fluid is completely full */
	public Block getBlockFull() { return ffluid.get(State.FULL); }
	
	/** should return the block to use when this fluid is placed as partially filled */
	public Block getBlockPartial() { return ffluid.get(State.PARTIAL); }
	
	/** Gets the level of this block from the metadata. Can be overridden to describe custom behavior. Should
	 * complement  {@link #getLevelFromMeta(int)}. */
	public int getLevelFromMeta(int m) {
		return (8 - m) * Settings.FLUID_LAYER;
	}
	
	/** Gets the metadata of this block from the level. Can be overridden to describe custom behavior. Should
	 * complement  {@link #getLevelFromMeta(int)}. */
	public int getMetaFromLevel(int l) { 
		return Math.max(0,  7 - (l / Settings.FLUID_LAYER));
	}
	
	//Can change this to 0 in order to sledgehammer lighting issues
	@Override public Block setLightOpacity(int n) { return super.setLightOpacity(2); }
	
	/** Get the tick rate for this block in the given world.
	 * <p>Override this method to specify custom behavior (i.e, Lava update at different rates in Overworld and Nether). */
	public int getTickRate(World w) { return tickRate; }

	/** The item representing a bucket of this block */
	public Item getBucket() { return Items.water_bucket; }
	
	/** <b>SHOULD  BE OVERRIDDEN FOR CUSTOM FLUIDS.</b><hr><p> {@inheritDoc}*/
	@Override public Fluid getFluid() { return ffluid.fluid; }
	
	/** Attempts to drain partial blocks <hr><p> {@inheritDoc} */
	@Override
	public FluidStack drain(World world, int x, int y, int z, boolean doDrain) 
	{
		/* I have no idea how this method is supposed to work :D */
		
		IWorld w = Manager.instance.getOrRegisterWorld(world);
		IChunk d = w.getOrRegisterChunk(world.getChunkFromChunkCoords(x >> 4, z >> 4));
		
		//calculate our level as 0-1000mB
		int level = Math.round((1000F / (float)Settings.FLUID_MAX) 
				  * (float) d.getFluid(this, -1, x & 0xF, y, z & 0xF));
		
		//real drain, empty this block
		if (canDrain(world,x,y,z))
		{
			//if (doDrain && canDrain(world, x, y, z))
			//{
				world.setBlockToAir(x, y, z);
				return new FluidStack(getFluid(), level);
			//}
			//else //simulated drain, should it return a fluid stack?
			//{
			//	return new FluidStack(getFluid(), level);
			//}
		}
		
		return null;
	}

	/** Should be fine if {@linkplain BlockFiniteFluid#drain(World, int, int, int, boolean)} is inct <hr><p> {@inheritDoc}*/
	@Override public boolean canDrain(World world, int x, int y, int z) { return true; }

	/** Fixes the issues with fluid heights of partial blocks <hr><p> {@inheritDoc}*/
	@Override
	public float getFilledPercentage(World world, int x, int y, int z) {
		if (y < 255 && world.getBlock(x, y + 1, z) instanceof BlockFiniteFluid) return 1.1F;
		return Math.max(0F, Math.min(1.1F, 0.1F + (float) getLevelFromMeta(world.getBlockMetadata(x, y, z)) / (float)Settings.FLUID_MAX));
	}
	
	
}
