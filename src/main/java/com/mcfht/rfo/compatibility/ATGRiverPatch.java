package com.mcfht.rfo.compatibility;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.RFOCommands;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.utils.Util;

import net.minecraftforge.common.config.Property;
import net.minecraftforge.common.config.Property.Type;

public class ATGRiverPatch extends Patch
{

	public static Class c = ATGRiverPatch.class;
	
	public static Property overrider;
	
	public static boolean INSTALLED = true;
	
	public ATGRiverPatch() {
		super("ATG", "ttftcuts.atg.config.configfiles.ATGMainConfig");
	}

	@Override
	public boolean performNecessarySteps() throws Exception 
	{
		if (INSTALLED)
		{
			Class c = Class.forName(className);
			overrider = new Property("Rivers", "false", Type.BOOLEAN, true);
			c.getDeclaredField("genRivers").set(c, overrider);
			Util.info("   - Blocked rivers");
		}
		return true;
	}
	
}
