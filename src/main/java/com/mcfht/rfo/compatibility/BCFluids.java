package com.mcfht.rfo.compatibility;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.RFOCommands;
import com.mcfht.rfo.RFORegistry;
import com.mcfht.rfo.RFORegistry.FFluid.State;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockCreativeSource;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.blocks.interactions.InteractionLavaFlammable;
import com.mcfht.rfo.utils.Util;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

/**
 * Replaces Buildcraft fluids with out own versions. So long as this runs before BC declares it's oil, the BC oil will
 * be replaced. This should be guaranteed by the load sort annotation in the IFMLLoadingPlugin implementor.
 * @author FHT
 */
public class BCFluids extends Patch 
{
	
	public static Class c = BCFluids.class;

	
	public static boolean PATCH_BC = true;
	
	public static boolean INSTALLED = false;
	
	/** The oil block we will use */
	public static BlockFiniteFluid blockOil;
	
	/** The fuel block we will use */
	public static BlockFiniteFluid blockFuel;
	
	//just use the buildcraft textures because it's easier
	public static final String[] fuelTex = {"BuildCraftEnergy:fluids/fuel_still", "BuildCraftEnergy:fluids/fuel_flow"};
	public static final String[] oilTex = {"BuildCraftEnergy:fluids/oil_still", "BuildCraftEnergy:fluids/oil_flow"};
	
	
	public BCFluids() {
		super("Buildcraft", "buildcraft.BuildCraftCore");
	}
	
	@Override public boolean performNecessarySteps() throws Exception 
	{
		if (PATCH_BC == false)
		{
			Util.info(" - skipping");
			INSTALLED = false;
			return true;
		}
		
		//if there is no oil registered 
		if (!FluidRegistry.isFluidRegistered("oil") || FluidRegistry.getFluid("oil").getBlock() == null);
		{
			blockOil = RFORegistry.registerFluid("oil", Material.water, 3.0F, 850, oilTex).get(State.FULL);
			RFORegistry.registerFluidInteraction("oil", "lava", new InteractionLavaFlammable(0.9F));
			RFORegistry.getFFluidByName("oil").setFireStats(15, 100);
			Util.info("   - Oil replaced");
		}
		
		//same as above, but for fuel
		if (!FluidRegistry.isFluidRegistered("fuel") || FluidRegistry.getFluid("fuel").getBlock() == null)
		{
			blockFuel = RFORegistry.registerFluid("fuel", Material.water, 0.6F, 800, fuelTex).get(State.FULL);
			RFORegistry.registerFluidInteraction("fuel", "lava", new InteractionLavaFlammable(2F));
			RFORegistry.getFFluidByName("fuel").setFireStats(30, 100);
			Util.info("   - Fuel replaced");
		}
		
		INSTALLED = true;
		return true;
	}

}
