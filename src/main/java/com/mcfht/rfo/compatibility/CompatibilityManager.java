package com.mcfht.rfo.compatibility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.mcfht.rfo.utils.Util;

public class CompatibilityManager 
{
	private static int _ct = 0;
	
	public static boolean INSTALLED = false;

	public static enum Stage
	{
		PRE("Pre-Initialization"),
		PERI("Initialization"),
		POST("Post-Initialization");
		
		final int val;
		final String name;
		Stage(String n)
		{
			val = _ct++;
			name = n;
		}
	}
	
	/** A list containing all the compatibility pathes to be used by this mod */
	private static ArrayList<Patch>[] patches = (ArrayList<Patch>[])new ArrayList[]
	{
		new ArrayList(), new ArrayList(), new ArrayList()
	};
	

    /** Registers a new compatibility patching instance. */
    public static void registerCompatibilityPatch(Patch p, Stage s)
    {
    	patches[s.val].add(p);
    }
    
	public static void performPatches(Stage s)
	{
		int count = 0;
		if (patches[s.val].size() > 0)
		{
			Util.enterSection("Applying " + s.name + " compatibility", 5);
	    	Util.info("");
			for (Patch p : patches[s.val])
			{
				if (p.checkCompatibility()) { count++; Util.info(""); }
			}
		}
		if (count <= 0) Util.info(" - nothing to patch!"); 
		Util.leaveSection();
	}
	

	
	
	
}
